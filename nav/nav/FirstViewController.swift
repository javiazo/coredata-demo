//
//  FirstViewController.swift
//  nav
//
//  Created by Javier Azorín Fernández on 21/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class FirstViewController: UIViewController {
    
    // outlets
    @IBOutlet weak var textLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else { return }
        
        switch segueId {
            case "second":
                let destVC = segue.destination as! SecondViewController
                destVC.text = textLabel.text
            default: break
        }
    }
    
    @IBAction func backFromSecondViewController(_ segue: UIStoryboardSegue) {
        guard let segueId = segue.identifier else { return }
        
        switch segueId {
            case "first":
                let sourceVC = segue.source as! SecondViewController
                textLabel.text = sourceVC.textField.text
            default: break
        }
    }
    
}

