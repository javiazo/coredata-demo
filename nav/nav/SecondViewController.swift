//
//  SecondViewController.swift
//  nav
//
//  Created by Javier Azorín Fernández on 21/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class SecondViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var textField: UITextField!
    
    var text: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.text = text
    }
}

extension SecondViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}
