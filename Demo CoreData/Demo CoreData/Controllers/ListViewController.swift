//
//  ListViewController.swift
//  Demo CoreData
//
//  Created by Javier Azorín Fernández on 22/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit
import CoreData

final class ListViewController: CoreDataTableViewController {
    
    // Segues
    fileprivate let segueDetail = "detail"

    // CoreData Stack
    fileprivate let stack = CoreDataStack.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create a fetchrequest
        let fr = NSFetchRequest<NSFetchRequestResult>(entityName: "Person")
        fr.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        // Create the FetchedResultsController
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fr, managedObjectContext: stack.context, sectionNameKeyPath: nil, cacheName: nil)
        
        // Remove unused separators
        tableView.tableFooterView = UIView()
    }
    
    // MARK: Add person
    @IBAction func addPerson(_ sender: AnyObject) {
        // Trigger segue
        performSegue(withIdentifier: segueDetail, sender: nil)
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else { return }
        switch segueId {
        case segueDetail:
            if let person = sender as? Person {
                let destNC = segue.destination as! UINavigationController
                let destVC = destNC.viewControllers.first as! DetailTableViewController
                destVC.person = person
            }
        default: break
        }
    }
    
}

// MARK: TableView DataSource
extension ListViewController {
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Find the right notebook for this indexpath
        let person = fetchedResultsController!.object(at: indexPath) as! Person
        
        // Create the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "person", for: indexPath)
        
        // Sync notebook -> cell
        cell.textLabel?.text = "\(person.name?.capitalized ?? "") \(person.surname?.capitalized ?? "")"
        cell.detailTextLabel?.text = "\(person.age > 0 ? "\(person.age) años" : "")"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if let context = fetchedResultsController?.managedObjectContext, let person = fetchedResultsController?.object(at: indexPath) as? Person, editingStyle == .delete {
            context.delete(person)
        }
    }
    
}

// MARK: TableView DataSource
extension ListViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Deselect
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        // Get person
        guard let person = fetchedResultsController?.object(at: indexPath) as? Person else { return }
        
        // Trigger segue
        performSegue(withIdentifier: segueDetail, sender: person)
    }
    
}
