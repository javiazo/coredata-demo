//
//  DetailTableViewController.swift
//  Demo CoreData
//
//  Created by Javier Azorín Fernández on 22/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class DetailTableViewController: UITableViewController {
    
    // Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    // Person
    var person: Person?
    
    // CoreData Stack
    fileprivate let stack = CoreDataStack.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove unused separators
        tableView.tableFooterView = UIView()
        
        // Load data
        loadData()
    }

    
    // Load data
    private func loadData() {
        // TextFields
        guard let p = person else { return }
        nameTextField.text = p.name
        surnameTextField.text = p.surname
        ageTextField.text = "\(p.age)"
    }
    
    // MARK: Done
    @IBAction func done(_ sender: AnyObject?) {
        dismiss(animated: true) {
            guard let name = self.nameTextField.text,
                let surname = self.surnameTextField.text,
                let age = self.ageTextField.text else {
                    return
            }
            
            if let p = self.person {
                p.name = name
                p.surname = surname
                p.age = Int64(age) ?? 0
            } else {
                let _ = Person(name: name, surname: surname, age: Int(age) ?? 0, context: self.stack.context)
            }
        }
    }

    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: TextField Delegate
extension DetailTableViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            surnameTextField.becomeFirstResponder()
        case surnameTextField:
            ageTextField.becomeFirstResponder()
        case ageTextField:
            textField.resignFirstResponder()
            done(nil)
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
