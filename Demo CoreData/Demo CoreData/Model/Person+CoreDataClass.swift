//
//  Person+CoreDataClass.swift
//  Demo CoreData
//
//  Created by Javier Azorín Fernández on 22/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import CoreData


final public class Person: NSManagedObject {

    convenience init(name: String, surname: String, age: Int, context: NSManagedObjectContext) {
        if let entity = NSEntityDescription.entity(forEntityName: "Person", in: context) {
            self.init(entity: entity, insertInto: context)
            
            self.name = name
            self.surname = surname
            self.age = Int64(age)
        } else {
            fatalError("Unable to find Entity name!")
        }
    }
    
}
