//
//  Person+CoreDataProperties.swift
//  Demo CoreData
//
//  Created by Javier Azorín Fernández on 22/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import CoreData


extension Person {

    @NSManaged public var name: String?
    @NSManaged public var surname: String?
    @NSManaged public var age: Int64

}
