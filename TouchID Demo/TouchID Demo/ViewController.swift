//
//  ViewController.swift
//  TouchID Demo
//
//  Created by Javier Azorín Fernández on 20/09/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

import LocalAuthentication

final class ViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var doorImageView: UIImageView!
    @IBOutlet weak var openButton: UIButton!
    
    // Authentication context
    fileprivate let context = LAContext()
    
    // Is door opened
    fileprivate var isDoorOpened = false {
        didSet {
            // Enable disable button
            openButton.isEnabled = !isDoorOpened
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure view
        configureView()
    }
    
    private func configureView() {
        // Door closed
        doorImageView.image = #imageLiteral(resourceName: "closed-door")
        
        // Close door action
        doorImageView.isUserInteractionEnabled = true
        doorImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeTheDoor)))
    }
    
    func closeTheDoor() {
        guard isDoorOpened else { return }
        
        DispatchQueue.main.async {
            self.doorImageView.image = #imageLiteral(resourceName: "closed-door")
            self.isDoorOpened = false
            
        }
    }
    
    @IBAction func login(_ sender: AnyObject) {
        // Authenticate
        authenticate()
    }
    
}

// Authentication
extension ViewController {
    
    fileprivate func authenticate() {
        // Error
        var error: NSError?
        
        // Check for TouchID availability
        guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            print("No Touch ID found!")
            return
        }
        
        // Ask for fingerprint
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Open de door!") { (success, error) in
            // Open (or not) the door
            if success {
                DispatchQueue.main.async {
                    self.doorImageView.image = #imageLiteral(resourceName: "opened-door")
                    self.isDoorOpened = true
                }

            }
        }
    }
  
}
